/* global FormView, SurveyView, SessionView */

var App = function() {
  this.form = new FormView();
  this.survey = new SurveyView();
  this.session = new SessionView();
};

App.prototype.init = function() {
  var self = this;

  $('#left-panel').panel();
  $('#left-panel ul[data-role="listview"]').listview();
  $('[data-role="header"]').toolbar();

  $('a[data-function="sync-forms"]').click(function() {
    self.form.synchronize();
  });

  $('a[data-function="list-forms"]').click(function() {
    self.form.list();
  });

  $('a[data-function="save-survey"]').click(function() {
    self.survey.save();
  });

  $('a[data-function="finish-survey"]').click(function() {
    self.survey.finish();
  });

  $('a[data-function="list-surveys"]').click(function() {
    self.survey.list();
  });

  $('a[data-function="send-surveys"]').click(function() {
    self.survey.list(null, true);
  });

  $('a[data-function="login"]').click(function() {
    $.mobile.navigate('#login');
  });

  $('#login-form').submit(function() {
    self.session.login();
    return false;
  });

  $('a[data-function="logout"]').click(function() {
    self.session.logout();
  });

  self.session.show();
};

App.prototype.closeLeftPanel = function() {
  $('#left-panel').panel('close');
};

App.prototype.error = function(error) {
  navigator.notification.alert(error.message, null, 'Error');
  console.error(error);
};

var app = new App();

$(document).on('deviceready', function() {
  app.init();
  window.onerror = function(message, source, lineno, colno, error) {
    app.error(error);
  };
});

// Setup Handlebars
Handlebars.registerHelper('getCollectionFieldValue', function(collection, index, field) {
  return collection.value[index][field.name];
});

Handlebars.registerHelper('getChecked', function(fieldValue, optionValue) {
  if ($.isArray(fieldValue)) {
    return $.inArray(String(optionValue), fieldValue) >= 0 ? 'checked' : null;
  }
  return String(fieldValue) === String(optionValue) ? 'checked' : null;
});

Handlebars.registerHelper('getSelected', function(fieldValue, optionValue) {
  return fieldValue === optionValue ? 'selected' : null;
});

Handlebars.registerHelper('momentCalendar', function(date) {
  return moment(date).calendar();
});

Handlebars.registerHelper('momentFromNow', function(date) {
  return moment(date).fromNow();
});

Handlebars.partials = Handlebars.templates;

// Setup MomentJS
moment.locale('es');
