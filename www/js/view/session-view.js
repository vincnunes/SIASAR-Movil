/* global Session, app, ApiError */

var SessionView = function() {
};

SessionView.prototype.show = function() {
  app.closeLeftPanel();
  var session = Session.get();
  if (session) {
    $('#home div.ui-content .session-info').html(Handlebars.templates['session-info']({
      session: session
    }));
    document.cookie = session.getCookieString();
    $('a[data-function="login"]').hide();
    $('a[data-function="logout"]').show();
    $('a[data-function="sync-forms"]').show();
    $('a[data-function="send-surveys"]').show();
    $.mobile.navigate('#home');
  }
};

SessionView.prototype.login = function() {
  var self = this;
  $.mobile.loading('show');
  Session.login($('#username').val(), $('#password').val()).then(function() {
    $.mobile.loading('hide');
    self.show();
  }).catch(function(error) {
    $.mobile.loading('hide');
    app.error(error);
  });
};

SessionView.prototype.logout = function() {
  app.closeLeftPanel();
  $.mobile.loading('show');
  Session.logout().then(function() {
    $('a[data-function="login"]').show();
    $('a[data-function="logout"]').hide();
    $('a[data-function="sync-forms"]').hide();
    $('a[data-function="send-surveys"]').hide();
    $('#home div.ui-content .session-info').empty();
    $('#forms-list div.ui-content .login-pre-sync').show();
    $.mobile.loading('hide');
    $.mobile.navigate('#home');
  }).catch(function(error) {
    $.mobile.loading('hide');
    app.error(error);
  });
};
