/* global Form, Log, app */

var FormView = function() {
};

FormView.prototype.synchronize = function() {
  var self = this;
  var log = new Log($('#sync-log'));
  app.closeLeftPanel();
  $.mobile.navigate('#forms-sync');
  Form.synchronize(log).then(function() {
    self.list();
  }).catch(function(error) {
    app.error(error);
  });
};

FormView.prototype.list = function() {
  app.closeLeftPanel();
  var forms = Form.list();
  if (forms && forms.length) {
    var content = $('#forms-list div.ui-content');
    content.html(Handlebars.templates['forms-list']({
      forms: forms
    }));
    content.find('ul[data-role="listview"]').listview();
    content.find('ul[data-role="listview"] li a').click(function(event) {
      app.survey.open($(event.target).data('id'));
    });
  }
  $.mobile.navigate('#forms-list');
};

FormView.prototype.clear = function() {
  Form.clear();
};
