/* global Survey, Form, app */

var SurveyView = function() {
};

SurveyView.prototype.save = function() {
  var self = this;
  self.getData();
  self._survey.save().then(function() {
    self.list();
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.finish = function() {
  var self = this;
  try {
    $('*').removeClass('error');
    self.getData(true);
    self._survey.finish().then(function() {
      self.list();
    }).catch(function(error) {
      app.error(error);
    });
  } catch (error) {
    error.element.addClass('error');
    $.mobile.silentScroll(error.element.offset().top - 100);
    navigator.notification.alert(error.message, null, error.title);
  }
};

SurveyView.prototype.open = function(formId, surveyId) {
  var self = this;

  Survey.get(surveyId).then(function(survey) {
    if (!survey) {
      survey = new Survey({
        form: Form.get(formId)
      });
    }
    self._survey = survey;

    var content = $('#survey-fill div.ui-content');
    content.html(Handlebars.templates['survey']({ // eslint-disable-line dot-notation
      survey: survey
    }));

    content.find('div[data-role="collapsibleset"]').collapsibleset();
    content.find('form').trigger('create');

    content.find('a[data-function="get-geolocation"]').click(function() {
      self.getGeolocation($(this).parent());
    });

    content.find('a[data-function="get-picture"]').click(function() {
      self.getPicture($(this).parent());
    });

    content.find('a[data-function="remove-picture"]').click(function() {
      self.removePicture($(this).parent());
    });

    content.find('a[data-function="add-collection"]').click(function() {
      self.addCollection($(this).parent());
    });

    content.find('a[data-function="remove-collection"]').click(function() {
      self.removeCollection($(this).parent());
    });

    $.each($('a[data-function="remove-collection"]'), function(index, button) {
      if ($(button).siblings('.collection-group').length > 1) {
        $(button).show();
      }
    });

    content.find('a[data-function="get-shs"]').click(function() {
      self.getShs($(this).parent(), 0);
    });

    $('#survey-fill div[data-role="footer"]').show();
    $.mobile.navigate('#survey-fill');
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.list = function(formId, finished) {
  var self = this;
  app.closeLeftPanel();
  Survey.list(formId, finished).then(function(surveys) {
    var content = $('#surveys-list div.ui-content');
    content.html(Handlebars.templates['surveys-list']({
      surveys: surveys,
      finished: finished
    }));
    content.find('ul[data-role="listview"]').listview();
    content.find('ul[data-role="listview"] li a').click(function() {
      if (finished) {
        self.send($(this).data('id'));
      } else {
        self.open($(this).data('form-id'), $(this).data('id'));
      }
    });
    $.mobile.navigate('#surveys-list');
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.getData = function(validate) {
  var formElement = $('#survey-fill form');
  var self = this;

  self._survey.data = {};

  this._survey.form.parse(function(field) {
    self._survey.data[field.name] = getFieldValue(field, formElement);
    if (validate) {
      validateField(field, self._survey.data[field.name], formElement);
    }
  }, function(collection) {
    self._survey.data[collection.name] = [];
    $.each($('div.collection-group[data-id="' + collection.name + '"]'), function() {
      self._survey.data[collection.name].push({});
    });
  }, function(field) {
    $.each(self._survey.data[field.collectionName], function(index) {
      var collection = $($('div.collection-group[data-id="' + field.collectionName + '"]')[index]);
      self._survey.data[field.collectionName][index][field.name] = getFieldValue(field, collection);
      if (validate) {
        validateField(field, self._survey.data[field.collectionName][index][field.name], collection);
      }
    });
  });

  function getFieldValue(field, context) {
    switch (field.type) {
      case 'geofield_latlon':
        return getGeoFieldValue(field, context);
      case 'options_buttons':
        return getOptionButtonsValue(field, context);
      case 'image_image':
        return getImageValue(field, context);
      case 'number':
        return getNumberValue(field, context);
      case 'mvf_widget_default':
        return getMVFValue(field, context);
      case 'taxonomy_shs':
        return getShsValue(field, context);
      default:
        return getTextValue(field, context);
    }
  }

  function getGeoFieldValue(field, context) {
    return {
      latitude: context.find('*[name="' + field.name + '_latitude' + '"]').val(),
      longitude: context.find('*[name="' + field.name + '_longitude' + '"]').val()
    };
  }

  function getOptionButtonsValue(field, context) {
    var value = $.map(context.find('*[name="' + field.name + '"]:checked'), function(option) {
      return $(option).data('value').toString();
    });
    if (value.length === 1) {
      value = value[0];
    }
    return value;
  }

  function getImageValue(field, context) {
    return context.find('*[name="' + field.name + '"]').attr('src');
  }

  function getNumberValue(field, context) {
    var value = context.find('*[name="' + field.name + '"]').val();
    if (value === '') {
      return null;
    }
    return Number(value);
  }

  function getTextValue(field, context) {
    var value = context.find('*[name="' + field.name + '"]').val();
    if (!value || value === '') {
      return null;
    }
    return value.trim();
  }

  function getMVFValue(field, context) {
    var value = getNumberValue(field, context);
    if (!value) {
      return null;
    }
    return {
      value: value,
      unit: context.find('*[name="' + field.name + '_unit"]').val()
    };
  }

  function getShsValue(field, context) {
    var value = getTextValue(field, context);
    if (!value) {
      return null;
    }
    return {
      value: value,
      label: context.find('input[name="' + field.name + '_label"]').val()
    };
  }

  function validateField(field, value, context) {
    if (field.required && value === null) {
      throw new RequiredError(field, context);
    }
    if (field.range && (
      (field.range.min !== null && value < field.range.min) ||
      (field.range.max !== null && value > field.range.max)
    )) {
      throw new OutOfRangeError(field, context);
    }
  }
};

SurveyView.prototype.send = function(id) {
  $.mobile.loading('show');
  Survey.get(id).then(function(survey) {
    survey.send().then(function() {
      $.mobile.navigate('#home');
      $.mobile.loading('hide');
    }).catch(function(error) {
      app.error(error);
      $.mobile.loading('hide');
    });
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.addCollection = function(fieldset) {
  var collections = fieldset.find('div.collection-group');
  var index = collections.length - 1;
  var clone = $(collections[index]).clone();

  clone.data('index', index + 1);
  clone.insertAfter($(collections[index]));

  fieldset.find('a[data-function="remove-collection"]').show();
};

SurveyView.prototype.removeCollection = function(fieldset) {
  var collections = fieldset.find('div.collection-group');
  var index = collections.length - 1;

  $(collections[index]).remove();
  if (index <= 1) {
    fieldset.find('a[data-function="remove-collection"]').hide();
  }
};

SurveyView.prototype.getPicture = function(field) {
  var image = field.find('img');
  navigator.camera.getPicture(function(picture) {
    if (picture.substring(0, 4) === 'file') {
      image.attr('src', picture);
    } else {
      image.attr('src', 'data:image/jpeg;base64,' + picture);
    }
    image.show();
    field.find('a[data-function="remove-picture"]').show();
  }, function(error) {
    app.error(error);
  }, {
    destinationType: navigator.camera.DestinationType.FILE_URI,
    sourceType: navigator.camera.PictureSourceType.CAMERA,
    saveToPhotoAlbum: true
  });
};

SurveyView.prototype.removePicture = function(field) {
  var image = field.find('img');
  image.attr('src', '');
  image.hide();
  field.find('a[data-function="remove-picture"]').hide();
};

SurveyView.prototype.getGeolocation = function(field) {
  var fieldName = field.data('id');
  $.mobile.loading('show');
  navigator.geolocation.getCurrentPosition(function(position) {
    $('#' + fieldName + '_latitude').val(position.coords.latitude);
    $('#' + fieldName + '_longitude').val(position.coords.longitude);
    $.mobile.loading('hide');
  }, function(error) {
    app.error(error);
    $.mobile.loading('hide');
  }, {
    timeout: 10000
  });
};

SurveyView.prototype.getShs = function(field, parent) {
  var self = this;
  var content = $('#shs-select div.ui-content');
  var shs = Form.getShs(field.data('shs'));
  content.html(Handlebars.templates['shs-select']({
    id: field.data('id'),
    options: shs[parent]
  }));
  content.find('ul[data-role="listview"]').listview();
  content.find('ul[data-role="listview"] li a').click(function() {
    if (shs[$(this).data('value')]) {
      self.getShs(field, $(this).data('value'));
    } else {
      field.find('input[type="hidden"]').val($(this).data('value'));
      field.find('input[type="text"]').val($(this).text());
      $.mobile.navigate('#survey-fill');
    }
  });
  $.mobile.navigate('#shs-select');
};

var ValidationError = function(field, context) {
  this.element = context.find('*[name="' + field.name + '"]');
};
ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

var RequiredError = function(field, context) {
  this.message = 'El campo "' + field.label + '" no puede estar vacío.';
  this.title = 'Campo requerido';
  ValidationError.call(this, field, context);
};
RequiredError.prototype = Object.create(ValidationError.prototype);
RequiredError.prototype.constructor = RequiredError;

var OutOfRangeError = function(field, context) {
  this.message = 'El valor del campo "' + field.label + '" está fuera de rango.';
  if (field.range.min !== null) {
    this.message += ' Mínimo: ' + field.range.min + '.';
  }
  if (field.range.max !== null) {
    this.message += ' Máximo: ' + field.range.max + '.';
  }
  this.title = 'Fuera de rango';
  ValidationError.call(this, field, context);
};
OutOfRangeError.prototype = Object.create(ValidationError.prototype);
OutOfRangeError.prototype.constructor = OutOfRangeError;
