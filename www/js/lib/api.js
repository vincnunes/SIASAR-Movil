/* global Session */

var Api = function() {
};

Api.url = 'http://staging.siasar.org/rest/';

Api.getData = function(url) {
  return new Promise(function(resolve, reject) {
    $.getJSON(url)
      .done(function(data) {
        resolve(data);
      })
      .fail(function(jqxhr) {
        reject(new ApiError(jqxhr.status, jqxhr.responseJSON[0]));
      });
  });
};

Api.sendData = function(url, data) {
  return new Promise(function(resolve, reject) {
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    if (Session.exists()) {
      headers['X-CSRF-Token'] = Session.get().token;
    }

    var config = {
      url: url,
      type: 'POST',
      headers: headers,
      dataType: 'json'
    };
    if (data) {
      config.data = JSON.stringify(data);
    }

    $.ajax(config).done(function(result) {
      resolve(result);
    }).fail(function(jqxhr) {
      reject(new ApiError(jqxhr.status, jqxhr.responseJSON[0]));
    });
  });
};

Api.getFormsList = function() {
  return Api.getData(Api.url + 'forms');
};

Api.getFormFields = function(form) {
  return Api.getData(Api.url + 'forms/' + form.id);
};

Api.getGroupStructure = function() {
  return Api.getData(Api.url + 'field_group_structure');
};

Api.getTaxonomyTerms = function() {
  return Api.getData(Api.url + 'taxonomy_term');
};

Api.getTaxonomyVocabulary = function() {
  return Api.getData(Api.url + 'taxonomy_vocabulary');
};

Api.getFieldCollections = function() {
  return Api.getData(Api.url + 'field_collection_structure');
};

Api.login = function(data) {
  return Api.sendData(Api.url + 'user/login', data);
};

Api.logout = function() {
  return Api.sendData(Api.url + 'user/logout', null);
};

Api.sendSurvey = function(data) {
  return Api.sendData(Api.url + 'entity_entityform', data);
};

Api.sendImage = function(data) {
  return this.sendData(this.url + 'entity_file', data);
};

var ApiError = function(status, message) {
  this.message = message || 'API request error';
  this.status = status;
};
ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;
