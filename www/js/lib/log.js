var Log = function(container) {
  this.container = container ? container : $('<div>');
  this.clear();
};

Log.prototype.add = function(string) {
  this.container.append($('<p>').text(string));
};

Log.prototype.clear = function() {
  this.container.empty();
};
