/* global Store */

var FormParser = function(collections, structure) {
  this._structure = structure;
  this._collections = collections;
  this._groups = [];
};

FormParser.prototype.parse = function(form, fields) {
  this._form = form;
  this._fields = fields;
  this._parseSections();
  return this._form;
};

FormParser.prototype._parseSections = function() {
  var self = this;
  self._form.sections = [];
  $.each(self._structure.entityform[self._form.id].form, function(groupIndex, group) {
    if (group.parent_name === '') {
      self._form.sections.push(group);
    }
  });
  self._form.sections = self._form.sections.map(function(section) {
    return self._parseGroup(self._structure.entityform[self._form.id].form[section.group_name]);
  });
  this._form.sections = this._sortItems(this._form.sections);
};

FormParser.prototype._parseGroup = function(data, isCollection) {
  var self = this;
  var group = self._parseGroupData(data);
  $.each(group.children, function(childIndex, child) {
    if (child.substring(0, 5) === 'group') {
      group.children[childIndex] = self._parseGroup(self._structure.entityform[self._form.id].form[child]);
    } else if (isCollection) {
      if (!self._collections[data.bundle][child]) {
        console.error('Field not found in collection. Form: ' + self._form.label + ' - Collection: ' + data.bundle + ' - Field: ' + child);
        return;
      }
      group.children[childIndex] = self._parseField(self._collections[data.bundle][child]);
    } else {
      if (!self._fields[self._form.id][child]) {
        console.error('Field not found. Form: ' + self._form.label + ' - Field: ' + child);
        return;
      }
      group.children[childIndex] = self._parseField(self._fields[self._form.id][child]);
    }
  });
  group.children = self._sortItems(group.children);
  return group;
};

FormParser.prototype._parseGroupData = function(data) {
  return {
    id: data.group_name,
    name: data.group_name,
    label: data.label,
    order: parseInt(data.weight, 10),
    children: data.children,
    type: 'group'
  };
};

FormParser.prototype._parseField = function(data) {
  var field = this._parseFieldData(data);
  if (field.module === 'options' || field.module === 'mvf' ||
    field.module === 'entityreference') {
    field.options = this._parseOptions(data);
  }
  if (field.module === 'shs') {
    field.options = this._parseMultilevelOptions(data);
  }
  if (field.module === 'field_collection') {
    field.collection = this._parseCollection(data);
  }
  if (field.module === 'number' || field.module === 'mvf') {
    field.range = this._parseRange(data);
  }
  return field;
};

FormParser.prototype._parseFieldData = function(data) {
  return {
    id: data.field_name,
    name: data.field_name,
    label: data.label,
    order: parseInt(data.widget.weight, 10),
    required: data.required === 1 ? true : false,
    type: data.widget.type,
    multiple: data.widget.cardinality === '1' ? false : true,
    module: data.widget.module,
    collectionName: data.entity_type === 'field_collection_item' ? data.bundle : null
  };
};

FormParser.prototype._parseRange = function(data) {
  var range = null;
  var max = null;
  var min = null;
  if (data.widget.type === 'number') {
    max = parseFloat(data.settings.max, 10);
    min = parseFloat(data.settings.min, 10);
  } else {
    max = parseFloat(data.settings.max.value, 10);
    min = parseFloat(data.settings.min.value, 10);
  }
  if (!isNaN(max) || !isNaN(min)) {
    range = {
      max: max,
      min: min
    };
  }
  return range;
};

FormParser.prototype._parseOptions = function(data) {
  return $.map(data.settings.allowed_values, function(value, index) {
    return {
      value: index,
      label: value
    };
  });
};

FormParser.prototype._parseMultilevelOptions = function(data) {
  var shs = 'localidad'; // TODO: replace by SHS identifier
  if (!Store.getInstance().getShs(shs)) {
    Store.getInstance().saveShs(shs, null);
    var options = {};
    $.each(data.settings.hierarchy, function(child, parent) {
      if (!options[parent]) {
        options[parent] = [];
      }
      options[parent].push({
        value: child,
        label: data.settings.allowed_values[child]
      });
    });
    Store.getInstance().saveShs(shs, options);
  }
  return {
    shs: shs,
    forceDeepest: data.widget.settings.shs.force_deepest === '1' ? true : false
  };
};

FormParser.prototype._parseCollection = function(data) {
  var self = this;
  var children = [];
  if (self._structure.field_collection_item[data.field_name]) {
    $.each(self._structure.field_collection_item[data.field_name].form, function(childIndex, child) {
      children.push(self._parseGroup(child, true));
    });
  } else {
    $.each(self._collections[data.field_name], function(childIndex, child) {
      children.push(self._parseField(child));
    });
  }
  return self._sortItems(children);
};

FormParser.prototype._sortItems = function(items) {
  return items.sort(function(a, b) {
    return a.order - b.order;
  });
};
