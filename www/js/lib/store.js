/* global Form, Survey, Session */

var Store = function() {
  if (window.openDatabase) {
    this.initDatabase();
  } else {
    throw new Error('Your device/browser does not support WebSQL.');
  }
};

Store.getInstance = function() {
  if (!Store.instance) {
    Store.instance = new Store();
  }
  return Store.instance;
};

Store.prototype.clearForms = function() {
  $.each(localStorage, function(key) {
    if (key !== 'session') {
      localStorage.removeItem(key);
    }
  });
  this.deleteSurveys();
};

Store.prototype.clear = function() {
  localStorage.clear();
};

Store.prototype.saveForms = function(forms) {
  var formsData = forms.map(function(form) {
    return {
      id: form.id,
      label: form.label,
      synchronized: form.synchronized
    };
  });
  localStorage.setItem('forms', JSON.stringify(formsData));
};

Store.prototype.getForms = function() {
  return JSON.parse(localStorage.getItem('forms'));
};

Store.prototype.saveForm = function(form) {
  localStorage.setItem('form_' + form.id, JSON.stringify(form));
};

Store.prototype.getForm = function(id) {
  return JSON.parse(localStorage.getItem('form_' + id));
};

Store.prototype.saveSession = function(session) {
  localStorage.setItem('session', JSON.stringify(session));
};

Store.prototype.getSession = function() {
  return JSON.parse(localStorage.getItem('session'));
};

Store.prototype.saveUser = function(user) {
  localStorage.setItem('user_' + user.id, JSON.stringify(user));
};

Store.prototype.getUser = function(id) {
  return JSON.parse(localStorage.getItem('user_' + id));
};

Store.prototype.clearSession = function() {
  localStorage.removeItem('session');
};

Store.prototype.transaction = function(sql, params, successFunction, errorFunction) {
  this.database.transaction(function(tx) {
    tx.executeSql(sql, params, function(queryTx, result) {
      if (successFunction) successFunction(result);
    }, function(queryTx, error) {
      if (errorFunction) {
        errorFunction(error);
      } else {
        throw error;
      }
    });
  }, function(error) {
    if (errorFunction) {
      errorFunction(error);
    } else {
      throw error;
    }
  });
};

Store.prototype.initDatabase = function() {
  this.database = window.openDatabase('siasapp', '1.0', 'SIASApp DataBase', 5 * 1024 * 1024);
  this.transaction('CREATE TABLE IF NOT EXISTS surveys ( \
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
    form_id TEXT NOT NULL, \
    name TEXT, \
    created_date TEXT NOT NULL, \
    updated_date TEXT, \
    data TEXT NOT NULL, \
    uploaded INTEGER NOT NULL, \
    finished INTEGER NOT NULL);', []);
};

Store.prototype.saveSurvey = function(survey) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var currentTime = (new Date()).toJSON();
    var data = JSON.stringify(survey.data);
    var query;
    var params;

    if (survey.id) {
      query = 'UPDATE surveys SET name = ?, updated_date = ?,  data = ?, finished = ?, uploaded = ? WHERE id = ?';
      params = [survey.name, currentTime, data, survey.finished ? 1 : 0, survey.uploaded ? 1 : 0, survey.id];
    } else {
      query = 'INSERT INTO surveys (form_id, name, created_date, updated_date, data, finished, uploaded) VALUES (?,?,?,?,?,?,?)';
      params = [survey.formId, survey.name, currentTime, currentTime, data, survey.finished ? 1 : 0, survey.uploaded ? 1 : 0];
    }

    self.transaction(query, params, function() {
      resolve(survey);
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.deleteSurveys = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'DELETE FROM surveys';
    self.transaction(query, [], function() {
      resolve();
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.getSurvey = function(id) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM surveys WHERE id = ?';
    self.transaction(query, [id], function(result) {
      if (result.rows.length) {
        resolve(self._parseSurveys(result.rows)[0]);
      } else {
        resolve();
      }
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.getSurveys = function(formId, finished) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM surveys';

    var filters = [
      'uploaded = 0'
    ];
    if (formId) {
      filters.push('form_id = ' + formId);
    }
    if (finished) {
      filters.push('finished = ' + (finished ? 1 : 0));
    }
    query += ' WHERE ' + filters.join(' AND ');

    self.transaction(query, [], function(result) {
      resolve(self._parseSurveys(result.rows));
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype._parseSurveys = function(rows) {
  var surveys = [];
  if (rows.length) {
    for (var i = 0; i < rows.length; i++) {
      var row = rows.item(i);
      surveys.push({
        id: row.id,
        formId: row.form_id,
        name: row.name,
        createdDate: row.created_date,
        updatedDate: row.updated_date,
        uploaded: row.uploaded  === 1,
        finished: row.finished === 1,
        data: JSON.parse(row.data)
      });
    }
  }
  return surveys;
};

Store.prototype.saveShs = function(id, options) {
  localStorage.setItem('shs_' + id, JSON.stringify(options));
};

Store.prototype.getShs = function(id) {
  return JSON.parse(localStorage.getItem('shs_' + id));
};
