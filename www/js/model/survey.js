/* global Store, Form, Session, Api */

var Survey = function(data) {
  Object.assign(this, data);
};

Survey.list = function(formId, finished) {
  return new Promise(function(resolve, reject) {
    Store.getInstance().getSurveys(formId, finished).then(function(surveys) {
      surveys = surveys.map(function(survey) {
        survey = new Survey(survey);
        survey.form = Form.get(survey.formId);
        return survey;
      });
      resolve(surveys);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.get = function(id) {
  return new Promise(function(resolve, reject) {
    Store.getInstance().getSurvey(id).then(function(survey) {
      if (survey) {
        survey = new Survey(survey);
        survey.form = Form.get(survey.formId);
        survey.form.parse(function(field) {
          field.value = survey.data[field.name];
        }, function(collection) {
          collection.value = survey.data[collection.name];
        }, function(field) {
          field.value = survey.data[field.collectionName][field.name];
        });
      }
      resolve(survey);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.prototype.save = function() {
  this._prepareSave();
  return Store.getInstance().saveSurvey(this);
};

Survey.prototype.finish = function() {
  this._prepareSave();
  this.finished = true;
  return Store.getInstance().saveSurvey(this);
};

Survey.prototype.send = function() {
  var self = this;
  var session = Session.get();
  var postedImages = [];

  return new Promise(function(resolve, reject) {
    if (!session) {
      reject(new Error('There is not active session.'));
      return;
    }
    if (!self.finished) {
      reject(new Error('Survey is not finished.'));
      return;
    }
    var date = Math.round(Date.now() / 1000);
    var data = {
      type: self.formId,
      user: session.user.id,
      created: date
    };
    var imagesToSend = [];
    self.form.parse(function(field) {
      // data[field.name] = getFieldValue(field, self.data[field.name]);
      if (field.type === 'image_image' && self.data[field.name] !== null) {
        imagesToSend.push(getImageId(field.name, self.data[field.name]));
      }
    });
    Promise.all(imagesToSend).then(function(postedImagesResult) {
      postedImages = postedImagesResult;
      self.form.parse(function(field) {
        data[field.name] = getFieldValue(field, self.data[field.name]);
      });
      Api.sendSurvey(data).then(function() {
        self.uploaded = true;
        Store.getInstance().saveSurvey(self).then(function() {
          resolve(self);
        });
      }).catch(function(error) {
        reject(error);
      });
    });
  });

  function getFieldValue(field, value) {
    var fieldValue = null;
    if (!value) {
      return fieldValue;
    }
    switch (field.type) {
      case 'geofield_latlon':
        fieldValue = getGeoFieldValue(field, value);
        break;
      case 'date_select':
        fieldValue = getDateSelectValue(field, value);
        break;
      case 'mvf_widget_default':
        fieldValue = getMVFValue(field, value);
        break;
      case 'image_image':
        fieldValue = getImageValue(field);
        break;
      case 'taxonomy_shs':
        fieldValue = value.value;
        break;
      default:
        fieldValue = value;
    }
    return fieldValue;
  }

  function getImageValue(field) {
    var fieldValue = null;
    var fid = searchFileId(field.name, postedImages);
    if (fid !== null) {
      fieldValue = { fid: fid};
    }
    return fieldValue;
  }

  function searchFileId(fieldName, postedImagesArray) {
    var fid = null;
    for (var i = 0; i < postedImagesArray.length; i++) {
      if (postedImagesArray[i].imageName === fieldName) {
        fid = postedImagesArray[i].result.fid;
      }
    }
    return fid;
  }

  function getImageId(name, value) {
    return new Promise(function(resolve, reject) {
      var imageBase64 = value.replace('data:image/jpeg;base64,', '');
      var dataImage = {
        file: imageBase64,
        uid: session.user.uid,
        filename: name + '.jpg'
      };
      Api.sendImage(dataImage).then(function(result) {
        var toReturn = {
          result: result,
          imageName: name
        };
        resolve(toReturn);
      }).catch(function(error) {
        reject('Error sending image: ' + error);
      });
    });
  }

  function getDateSelectValue(field, value) {
    return Math.round(Date.parse(value) / 1000);
  }

  function getMVFValue(field, value) {
    return {
      'value': value.value,
      'target_id': value.unit
    };
  }

  function getGeoFieldValue(field, value) { // It does not work propertly
    return {
      'geom': 'POINT (' + value.longitude + ' ' + value.latitude + ')',
      'geo_type': 'point',
      'lat': value.latitude,
      'lon': value.longitude
    };
  }
};

Survey.prototype._prepareSave = function() {
  this.finished = false;
  this.uploaded = false;
  this.name = this.data.field_entity_name;
  this.formId = this.form.id;
};
