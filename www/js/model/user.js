/* global Store */

var User = function(data) {
  Object.assign(this, data);
};

User.prototype.save = function() {
  Store.getInstance().saveUser(this);
};

User.get = function(id) {
  Store.getInstance().getUser(id);
};
