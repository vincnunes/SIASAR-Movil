/* global Store, Api, Log, FormParser */

var Form = function(data) {
  Object.assign(this, data);
};

Form.prototype.save = function(form) {
  if (form) {
    Object.assign(this, form);
  }
  Store.getInstance().saveForm(this);
};

Form.list = function() {
  var forms = Store.getInstance().getForms();
  if (forms) {
    forms = forms.map(function(form) {
      return new Form(form);
    });
  }
  return forms;
};

Form.saveForms = function(forms) {
  forms.forEach(function(form) {
    form.save();
  });
  Store.getInstance().saveForms(forms);
};

Form.get = function(id) {
  return new Form(Store.getInstance().getForm(id));
};

Form.getForms = function(log) {
  return new Promise(function(resolve, reject) {
    if (log) log.add('Obteniendo listado de formularios...');
    Api.getFormsList().then(function(data) {
      var forms = $.map(data, function(value, index) {
        return new Form({
          id: index,
          label: value,
          synchronized: false
        });
      });
      Form.saveForms(forms);
      resolve(forms);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.getFields = function(form, log) {
  return new Promise(function(resolve, reject) {
    if (log) log.add('Descargando campos del formulario "' + form.label + '"...');
    Api.getFormFields(form).then(function(fields) {
      resolve(fields);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.getGroupStructure = function(log) {
  return new Promise(function(resolve, reject) {
    if (log) log.add('Descargando estructuras de grupo...');
    Api.getGroupStructure().then(function(structure) {
      resolve(structure);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.getFieldCollections = function(log) {
  return new Promise(function(resolve, reject) {
    if (log) log.add('Descargando colecciones de campos...');
    Api.getFieldCollections().then(function(collections) {
      resolve(collections);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.synchronize = function(log) {
  Store.getInstance().clearForms();
  return new Promise(function(resolve, reject) {
    if (log) log.add('Sincronizando formularios...');
    Promise.all([Form.getForms(log), Form.getFieldCollections(log), Form.getGroupStructure(log)]).then(function(data) {
      var fieldsPromises = data[0].map(function(form) {
        return Form.getFields(form, log);
      });
      var parser = new FormParser(data[1], data[2]);
      Promise.all(fieldsPromises).then(function(fieldsData) {
        data[0].forEach(function(form, index) {
          if (log) log.add('Procesando formulario "' + form.label + '"...');
          parser.parse(form, fieldsData[index]);
          form.synchronized = true;
        });
        Form.saveForms(data[0]);
        resolve(data[0]);
      });
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.prototype.parse = function(fieldFunction, collectionFunction, collectionFieldFunction) {
  var self = this;
  $.each(self.sections, function(sectionIndex) {
    self.parseGroup(self.sections[sectionIndex].children, fieldFunction, collectionFunction, collectionFieldFunction);
  });
};

Form.prototype.parseGroup = function(group, fieldFunction, collectionFunction, collectionFieldFunction) {
  var self = this;
  $.each(group, function(childIndex, child) {
    switch (child.type) {
      case 'group':
        self.parseGroup(group[childIndex].children, fieldFunction, collectionFunction, collectionFieldFunction);
        break;
      case 'field_collection_embed':
        if (collectionFunction) {
          collectionFunction(group[childIndex]);
        }
        if (collectionFieldFunction) {
          self.parseGroup(group[childIndex].collection, collectionFieldFunction, collectionFunction, collectionFieldFunction);
        } else {
          self.parseGroup(group[childIndex].collection, fieldFunction, collectionFunction, collectionFieldFunction);
        }
        break;
      default:
        if (fieldFunction) {
          fieldFunction(group[childIndex]);
        }
    }
  });
};

Form.getShs = function(id) {
  return Store.getInstance().getShs(id);
};
