/* global Store, Api, User */

var Session =  function(data) {
  Object.assign(this, data);
};

Session.prototype.getCookieString = function() {
  return this.sessionName + '=' + this.sessionId;
};

Session.prototype.save = function() {
  Store.getInstance().saveSession(this);
};

Session.login = function(username, password) {
  return new Promise(function(resolve, reject) {
    Api.login({
      'name': username,
      'pass': password
    }).then(function(data) {
      var user = new User({
        id: data.user.uid,
        username: data.user.name,
        email: data.user.mail,
        language: data.user.language,
        name: data.user.field_nombre_completo.length ? data.user.field_nombre_completo[user.language][0].safe_value : data.user.name
      });
      var session = new Session({
        sessionId: data.sessid,
        sessionName: data.session_name,
        token: data.token,
        user: user
      });
      session.save();
      resolve(session);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Session.logout = function() {
  return new Promise(function(resolve, reject) {
    var session = Store.getInstance().getSession();
    Api.logout(session).then(function(data) {
      resolve(data);
    }).catch(function(error) {
      reject(error);
    });
    Store.getInstance().clearSession();
  });
};

Session.get = function() {
  var session = Store.getInstance().getSession();
  if (session) {
    session = new Session(session);
    session.user = new User(session.user);
  }
  return session;
};

Session.getUser = function() {
  return Session.get().user;
};

Session.exists = function() {
  return Store.getInstance().getSession() ? true : false;
};
