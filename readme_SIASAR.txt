*****************************************************************
* SIASApp Project a.k.a. SIASAR 2.0 Mobile which belongs to SIASAR
* World Bank Project (www.siasar.org)
* 
* Developed by SIASAR Mobile development group:
*  + Jamal Toutouh, PhD <jamaltoutouh@gmail.com> 
*  + Eder Quintela <ederquintela@gmail.com> 
*  + Vinicius Nunes <vincnunes@gmail.com>
*  + Ra�l Orue <euroluar@gmail.com>
*  + Pedro Leiva Cerdas <pleiva@aya.go.cr>
*****************************************************************

This folder contains the files to develop SIASApp.

It has been developed using the following JavaScript libraries:
+ JQuery 1.12.3
+ JQuery Mobile 1.4.5
+ IR A�ADIENDO LOS QUE SE VAYAN USANDO (THIRD PART JS)

and CSS files:
+ JQuery Flat UI Style

In turn, we have developed the following JavaScript files to include some specific functionalities:
+ IR INCLUYENDO AQU� LOS QUE VAYAMOS HACIENDO



